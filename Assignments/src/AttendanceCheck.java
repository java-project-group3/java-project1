public class AttendanceCheck {

	public static void main(String[] args) {
		Attendance obj = new Attendance();

		String attendance1[] = {"yy", "nn"};
		String attendance2[] = {"nyy"};
		String attendance3[] = {"ynyy", "yyyy", "yyyy", "yyny", "nyyn"};
		String attendance4[] = {"yyy", "yyy", "ynn", "yyn", "yyn"};

		System.out.println(obj.maxStreak(2, 2, attendance1));   //1
		System.out.println(obj.maxStreak(3, 1, attendance2));   //0
		System.out.println(obj.maxStreak(4, 5, attendance3));   //2
		System.out.println(obj.maxStreak(3, 5, attendance4));   //2
	}

}