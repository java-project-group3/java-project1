package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class ArrayList1 {

	public static void main(String[] args) {
		
		ArrayList<Integer> al = new ArrayList<Integer>();
		al.add(10);
		al.add(20);
		al.add(5);
		al.add(15);
		al.add(8);
		
		Collections.sort(al);
		System.out.println(al);
		
		Collections.sort(al,Collections.reverseOrder());
		System.out.println(al);
		
		Iterator<Integer> it = al.iterator();
		int count=0;
		
//		while(it.hasNext()) {
//			int x = it.next();
//			if(x%2==0) {
//				count++;
//			}
//			}
//		System.out.println(count);
		
		//int count =0;
		for(int s:al) {
			if(s%2==0) {
				count++;
			}
		}
		System.out.println(count);
		

	}

}
