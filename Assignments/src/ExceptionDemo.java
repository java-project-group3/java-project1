package Exceptions;

import java.util.Scanner;

public class ExceptionDemo {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("enter a,b values");
		int a = scan.nextInt();
		int b = scan.nextInt();
		
		int[] c = new int[10];
		
		try {
		System.out.println(a+b);
		System.out.println(a*b);
		System.out.println(a/b);
		}
		catch(Exception e) {
			System.out.println(e);
		}
		finally {
			scan.close();
			System.out.println("closed");
		}

	}

}
