import java.util.Scanner;

public class StrongNumber {
 
	
	public static int factorial(int n){
		int i,fact=1;
		for(i=n;i>0;i--){
			fact = fact*i;
		}
		return fact;
	}
	public static boolean isStrongNumber(int num){
		int temp =num,sum=0,digit;
		boolean flag =false;
		while(temp!=0){
			digit=temp%10;
			sum = sum+factorial(digit);
			temp=temp/10;
		}
		if(sum==num){
			flag=true;
		}
		else{
			flag=false;
		}
		return flag;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
int n;
Scanner sc = new Scanner(System.in);
System.out.println("Enter any number");
n = sc.nextInt();
boolean res=isStrongNumber(n);
if(res==true){
	System.out.println(n+" is a strong number");
}
else
{
	System.out.println(n+" is  not a strong number");
	}
	}

}
