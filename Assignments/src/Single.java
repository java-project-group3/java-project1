package Inheritance;

class Demo{
	
	int a = 10;
	public void method() {
		System.out.print("good ");
	}
}

class Demo2 extends Demo {
	public void method2() {
		System.out.println("Morning ");
	}
}
public class Single {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Demo2 d = new Demo2();
		System.out.println(d.a);
		d.method();
		d.method2();

	}

}
